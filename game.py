from random import randint


name = input("Hello! What is your name: ")
#ask for the users name and have them enter their name to start


for guess_number in range(1, 6):
  guess_month = randint(1, 12)
  guess_year = randint(1924, 2004)

  guess = name + " were you born in " + str(guess_month) + " / " + str(guess_year) + "?"

  print(guess)
  answer = input("Yes or no? ")

  if answer == "Yes":
    print("I knew it")
    exit()
  elif guess_number == 5:
    print("I have other things to do. Bye bye.")
  else:
    print("Drats! Let me try that again then!")
